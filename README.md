Drupal OpenShift Example Project
================================

This repo is an example project for setting up a Drupal website in OpenShift using the OIT Drupal templates.

The OIT Drupal templates are designed to work with the [Red Hat PHP Source to Image](https://github.com/sclorg/s2i-php-container) ImageStreams, and follows the conventions of these ImageStreams to install Drupal via a [composer.json](composer.json) file.

This repo is based on the [Drupal-Composer/Drupal-Project repo](https://github.com/drupal-composer/drupal-project), and serves as an example only.  Real Drupal deployments in OpenShift would use their own repos in conjunction with the OIT Drupal templates for OpenShift.

## Installing Drupal 

A new Drupal application can be installed either via a web browser and the OpenShift Console, or the command line, with the `oc` binary: [OpenShift Origin Client Tools](https://github.com/openshift/origin/releases/latest).

After logging into the cluster, and changing to the project for the new Drupal deployment, a new Drupal application can be deployed.

_Note:_ If the repo with the Drupal code requires authentication, follow the steps in [Creating Deploy Secrets](deploy-secrets.md) section before moving to the steps below.

```
# Deploy a Drupal site using the OIT Drupal Templates for OpenShift

# SOURCE_REPOSITORY_URL should be a git repo in one of the following forms:
#
# To clone with a deploy key:
# ssh://git@<git_server>/<project>/<repo>.git
#
# To clone a repo without auth:
# https://<git_server>/<project>/<repo>.git

# SOURCE_REPOSITORY_REF should be the branch/ref name you wish to clone
# ie: master, v1.0, etc

# DOCUMENTROOT should be the path relative the repository root where the 
# Drupal files will exist after composer builds the site, eg: /web

# NAME should be a name for your project, eg: web-prod, my-first-drupal, etc.
# this is NOT the URL for the website.

oc new-app --template=drupal \
           --param=SOURCE_REPOSITORY_URL=${SOURCE_REPOSITORY_URL} \
           --param=SOURCE_REPOSITORY_REF=${SOURCE_REPOSITORY_REF} \
           --param=DOCUMENTROOT=${DOCUMENTROOT} \
           --param=NAME="${NAME}"

```

After running the new-app command, you can follow the progress of the OpenShift build my running `oc logs -f bc/${NAME}` where $NAME is the name used in the new-app command above.

Changes will also be reflected live in the OpenShift web console overview for the project.




